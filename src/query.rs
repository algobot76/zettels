//Copyright (c) 2020 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/19176983/zettels.png")]
//
//! Module to handle queries to the Zettelkasten.
//!
use std::collections::{HashSet};
use std::path::PathBuf;
use clap::ArgMatches;
use args;

use libzettels::{Config, Index};


/// Processes queries of the user. Takes a reference to the index, 
/// the user argumentes (`matches`) and a reference to the configuration.
/// Returns a HashSet containing the paths of matching zettels relative to 
/// the root directory as well as the implicit `pretty` flag for output.
/// Pretty is set to `true` if the whole zettelkasten turns out to be the 
/// scope of the query.
pub fn query(index: &Index, matches: &ArgMatches, cfg: &Config) 
            -> (HashSet<PathBuf>, bool) {
    
    // initialize the pretty flag, that might be set implicitly
    let mut pretty = false;
    
    // ------------------------------------------
    // SCOPE
    // ------------------------------------------
    // For inspection, we need scope. But if we search, we use the
    // search result for scope, ignoring provided scope arguments.
    // So let's get provided scope arguments first.
    // But we need it as a HashSet<PathBuf>
    
    let mut scope = HashSet::new();
    for arg in args::get_scope(&matches, "SCOPE") {
        scope.insert(PathBuf::from(arg));
    }
    debug!("SCOPE arguments: {:?}", scope);
    
    // ----------------------------------------
    // Now, let's SEARCH
    // ----------------------------------------
    // We prepare an empty tuple for searchterms
    // The first element is for keywords, the second for title_searchterms
    let mut search_flag = false;
    let mut exact = false;
    let mut searchterms = (vec![], String::new());
        
    if matches.is_present("keywords") {
        search_flag = true;
        searchterms.0 = args::values_to_vec(matches.values_of("keywords"));
    }
    if matches.is_present("title") {
        search_flag = true;
        searchterms.1 = matches.value_of("title").unwrap().to_string();
    } else if matches.is_present("exacttitle") {
        search_flag = true;
        exact = true;
        searchterms.1 = matches.value_of("exacttitle").unwrap().to_string();
    }
    let all = matches.is_present("all");
        
    // OK, let's actually search and replace scope with our results.
    if search_flag {
        scope = run_search(&index, searchterms, exact, all);
    } else {
    // However, if we haven't searched, we have to check whether the user
    // provided scope. If not, replace scope with the whole zettelkasten:
        if scope.is_empty() {
            // We set the pretty flag implicitly
            debug!("'pretty'-flag set implicitly.");
            pretty = true;
            for (key, _) in &index.files {
                scope.insert(key.to_path_buf());
            }
        }
    }
    debug!("SCOPE after possible search: {:?}", scope);
       
    // -------------------------------------------
    // INSPECT
    // -------------------------------------------
    
    let mut inspection_flag = false;
    let mut inspection = HashSet::new();
    
    
    if matches.is_present("sequences") {
        // sequences
        inspection_flag = true;
        inspection = index.sequences(&scope, &cfg.sequencestart);
    } else if matches.is_present("zettels") {
        // zettels (=zettels_of_sequence)
        inspection_flag = true;
        for sequence in &scope {
            let mut temp = index.zettels_of_sequence(sequence);
            for zettel in temp.drain() {
                inspection.insert(zettel);
            }
        }
    } else if matches.is_present("family") {
        // family (=sequence_tree)
        inspection_flag = true;
        inspection = index.sequence_tree(&scope);
    } else if matches.is_present("wholefamily") {
        // wholefamily (=sequence_tree_whole)
        inspection_flag = true;
        inspection = index.sequence_tree(&scope);
    }
    
    //    - links
    if matches.is_present("links") {
        inspection_flag = true;
        inspection = index.inspect_links(&scope);
    }
    //    - incoming
    if matches.is_present("incoming") {
        inspection_flag = true;
        inspection = index.inspect_incoming_links(&scope, all);
    }

    // OK, now I still have scope and I have inspection. which one to 
    // return?
    // If I have inspected: inspection
    // If I have searched, but not inspected: scope
    // If I have done neither: scope
    // If the user has not provided scope, scope is the whole zettelkasten
    
    if inspection_flag {
        return (inspection, pretty);
    }
    // else, return:
    (scope, pretty)
}

pub fn get_files(index: &Index) -> HashSet<PathBuf> {
    let mut scope = HashSet::new();
    for (key, _) in &index.files {
        scope.insert(key.to_path_buf());
    }
    scope
}

// -------------------------------------------------------------------------
// Private functions
// -------------------------------------------------------------------------

fn run_search(index: &Index, 
              searchterms: (Vec<String>, String),
              exact: bool,
              all: bool) -> HashSet<PathBuf> {
    debug!("Search for {:?}", searchterms);
    
    let mut results = HashSet::new();
    
    if !searchterms.0.is_empty() && !searchterms.1.is_empty() {
        // combisearch
        results = index.combi_search(searchterms.0, all,
                                     searchterms.1, exact);
    } else if !searchterms.0.is_empty() {
        // keyword search only
        results = index.search_keywords(searchterms.0, all);
    } else if !searchterms.1.is_empty() {
        // title search only
        results = index.search_title(searchterms.1, exact)
    }
    results
}