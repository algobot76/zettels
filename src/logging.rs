//Copyright (c) 2020 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/19176983/zettels.png")]
// --------------------------------------------------------------------------

use std::fs::File;
use std::path::{Path};
use simplelog::{CombinedLogger, 
                TermLogger, 
                WriteLogger,
                LevelFilter, 
                TerminalMode,
                Config as LogConfig};
                
// --------------------------------------------------------------------------
                
// ##################################
// #         Logging                #
// ##################################
/// Sets up the logging. Writes a log to "zettels.log" in the configuration
/// directory specified by `config_dir` with the logging level specified 
/// by `level`.
/// - Level 0: Error
/// - Level 1: Warn
/// - Level 2: Info
/// - Level 3: Debug
/// - Level 4: Trace
pub fn setup_logging<P: AsRef<Path>>(level: u64, config_dir: P) {
    let config_dir = config_dir.as_ref();

    
    let term_level = get_levelfilter(level);
    let write_level = get_levelfilter(level+1);
    let log_file = config_dir.join("zettels.log");
    // uncomment this for development and debugging
    //let log_file = std::env::current_dir()
    //                    .unwrap().join("dev-log/zettels.log");
    
    CombinedLogger::init(
        vec![
            TermLogger::new(term_level, 
                            LogConfig::default(), 
                            TerminalMode::Mixed).unwrap(),
            WriteLogger::new(write_level, 
                             LogConfig::default(), 
                             File::create(&log_file).unwrap()),
            ]
        ).unwrap();
    debug!("Set logging level to: {:?}", term_level);
    debug!("Writing logs at level: {:?} to {:?}.", write_level, log_file);
}

fn get_levelfilter(level: u64) -> LevelFilter {
    match level {
        0 => LevelFilter::Error,    // --mute
        1 => LevelFilter::Warn,     
        2 => LevelFilter::Info,     // -v   verbose
        3 => LevelFilter::Debug,    // -vv  very verbose
        _ => LevelFilter::Trace,    // -vvv very very verbose
    }
}