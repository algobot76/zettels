//Copyright (c) 2020 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/19176983/zettels.png")]
//
//! Module to generate a set of example files.
//!
use libzettels;
use std::io;
use std::path::PathBuf;
use clap::ArgMatches;

/// Generates a set of example files based on user arguments contained
/// in `matches`.
/// - bare: Just the example zettel files
/// - with_config: bare + a config file
/// - with_index: with_config + a pre-generated index file.
pub fn go(matches: &ArgMatches) {
    let dir = ask_dir();
    let mut config = true;
    if matches.is_present("with_config") {
        info!("Generating examples with config file.");
        libzettels::examples::generate_examples_with_config(&dir)
            .expect("Failed to generate examples:");
    } else if matches.is_present("with_index") {
        info!("Generating examples with config file and index.");
        libzettels::examples::generate_examples_with_index(&dir)
            .expect("Failed to generate examples:");
    } else {
        config = false;
        info!("Generating bare examples.");
        libzettels::examples::generate_bare_examples(&dir)
            .expect("Failed to generate examples:");
    }
    println!("Example files written to `examples`.");
    if config {
        println!("Example config files written to `example_config`.");
    }
    std::process::exit(0);
}

fn ask_dir() -> PathBuf {
    println!("This will generate the examples directory inside the current \
            directory. \
            Proceed? (y/n)");
    let cwd;
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read line");
    if answer.trim().eq_ignore_ascii_case("y") {
        cwd = std::env::current_dir()
                        .expect("Failed to determine current directory.")
    } else {
        warn!("Aborted by user");
        std::process::exit(0);
    }
    debug!("Returning as dir: {:?}", cwd);
    cwd
} 