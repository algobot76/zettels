//Copyright (c) 2020 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/19176983/zettels.png")]

//! Module for handling user arguments

use clap::{Arg, ArgMatches, App, SubCommand, Values};
use atty::Stream;
use std::io::{self, BufRead};

// --------------------------------------------------------------------------
/// Defines the user arguments.
pub fn app<'a, 'b>() -> App<'a, 'b> {
    App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("A command line tool to work with Niklas Luhmann's \
                Zettelkasten-system")
        .long_about("A command line tool to work with Niklas Luhmann's \
    Zettelkasten-system.\nZettels has three kinds of options:
    1. Search options  (-k, -t, -e, -a)
      – used to find (an) entry point(s) into the Zettelkasten.
    2. Inspect options (-s, -z, -f, -w, -l, -i)
      – used to inspect relations of one or more zettels to others (requires 
      at least one SCOPE argument).
      '-l' and '-i' can be combined with all other inspect options.
    3. General options (all others)
    \n\
    If Search and Inspect options are mixed, the search is conducted first, \
    and the result is used as the SCOPE for the inspection, ignoring provided \
    SCOPE arguments. \
    If neither search nor inspect options are set, zettels will output \
    SCOPE (if any) or the \n\
    whole Zettelkasten, implying the `--pretty` flag.")
        .subcommand(SubCommand::with_name("setup")
            .version(crate_version!())
            .about("Interactively generate a new config file or show \
                existing configuration.")
            .display_order(80)
            .arg(Arg::with_name("show")
                .short("s")
                .long("show")
                .help("Only show existing configuration.")
                .display_order(10)
                )
            )
        .subcommand(SubCommand::with_name("list")
            .version(crate_version!())
            .about("list keywords, titles or filenames.")
            .display_order(85)
            .arg(Arg::with_name("list_keywords")
                .conflicts_with_all(&["list_titles", 
                                      "list_files", 
                                      "list_keyword_count"])
                .short("k")
                .long("keywords")
                .display_order(10)
                .help("List keywords"))
            .arg(Arg::with_name("list_keyword_count")
                .conflicts_with_all(&["list_titles", 
                                      "list_files",
                                      "list_keywords"])
                .short("n")
                .long("numbers")
                .display_order(11)
                .help("List keyword counts"))
            .arg(Arg::with_name("list_titles")
                .short("t")
                .long("titles")
                .display_order(12)
                .help("List titles"))
            .arg(Arg::with_name("list_files")
                .short("f")
                .long("files")
                .display_order(13)
                .help("List files"))
            )
        .subcommand(SubCommand::with_name("examples")
            .version(crate_version!())
            .about("Generate example files")
            .display_order(90)
            .arg(Arg::with_name("bare")
                .long("bare")
                .display_order(10)
                .help("Example files only. Default.")
                .long_help("Only generates a bunch of example files in \n\
                the Zettelkasten directory, intended to test/try starting \n\
                from scratch, i.e. run setup.")
                )
            .arg(Arg::with_name("with_config")
                .long("with_config")
                .display_order(20)
                .help("Example files with config file.")
                .long_help("Generates example files and a configuration \n\
                file (in YAML), intended to test/try creating a fresh index \n\
                without going through the setup process.")
                )
            .arg(Arg::with_name("with_index")
                .long("with_index")
                .display_order(30)
                .help("Example files with config file and index.")
                .long_help("Generates example files, a config file and a \n\
                ready-made index file, intended to test/try using the \n\
                Zettelkasten (e.g. querying, updating etc.)")
                )
            )
        .arg(Arg::with_name("keywords")
            .short("k")
            .long("keywords")
            .value_name("KEYWORD")
            .takes_value(true)
            .multiple(true)
            .display_order(10)
            .help("Output Zettels tagged with KEYWORD."))
        .arg(Arg::with_name("title")
            .short("t")
            .long("title")
            .value_name("SEARCHTERM")
            .takes_value(true)
            .display_order(11)
            .conflicts_with("exacttitle")
            .help("Output Zettels whose titles contains SEARCHTERM."))
        .arg(Arg::with_name("exacttitle")
            .short("e")
            .long("exacttitle")
            .value_name("SEARCHTERM")
            .takes_value(true)
            .display_order(12)
            .conflicts_with("title")
            .help("Output Zettels whose titles match SEARCHTERM exactly."))
        .arg(Arg::with_name("sequences")
            .short("s")
            .long("sequences")
            .conflicts_with_all(&["zettels", "family", "wholefamily"])
            .display_order(10)
            .help("Output sequences to which the zettels specified by SCOPE belong"))
        .arg(Arg::with_name("zettels")
            .short("z")
            .long("zettels")
            .conflicts_with_all(&["family", "wholefamily"])
            .display_order(10)
            .help("Output zettels that belong to the sequence(s) specified by SCOPE."))
        .arg(Arg::with_name("family")
            .short("f")
            .long("family")
            .conflicts_with("wholefamily")
            .display_order(11)
            .help("Output zettels that belong to the sequence(s) specified by SCOPE,\
\nplus all ancestors of SCOPE."))
        .arg(Arg::with_name("wholefamily")
            .short("w")
            .long("wholefamily")
            .display_order(12)
            .help("Output zettels that belong to the sequence(s) specified by SCOPE,\
\nplus all ancestors of SCOPE, plus all descendants of those ancestors."))
        .arg(Arg::with_name("links")
            .short("l")
            .long("links")
            .display_order(13)
            .help("Output zettels to which the zettels specified by SCOPE link to."))
        .arg(Arg::with_name("incoming")
            .short("i")
            .long("incoming")
            .display_order(14)
            .help("Output all Zettels that link to the zettels specified \
                    by SCOPE."))
        .arg(Arg::with_name("SCOPE")
            .multiple(true)
            .long_help(
"Path(s) to one or several Zettel files to search or inspect.\n\
Paths must either be absolute, relative to the Zettelkasten's 
root dir, or relative to the current working directory.\n
If no scope is given, the whole Zettelkasten is the scope.")
            .required(false)
            .index(1))
        // General Options
        .arg(Arg::with_name("update")
            .short("u")
            .long("update")
            .global(true)
            .display_order(20)
            .help("Update the index before doing anything else."))
        .arg(Arg::with_name("all")
            .short("a")
            .long("all")
            .display_order(21)
            .help("Output zettels that match ALL queries."))
        .arg(Arg::with_name("pretty")
            .short("p")
            .long("pretty")
            .display_order(22)
            .help("Format output as 'title | file'"))
        .arg(Arg::with_name("v")
            .short("v")
            .multiple(true)
            .display_order(90)
            .global(true)
            .help("Increase verbosity.")
            .long_help("Increase verbosity. Can be used multiple times, up to \
                      '-vvv' (very very verbose)."))
        .arg(Arg::with_name("mute")
            .short("m")
            .long("mute")
            .display_order(91)
            .help("Supresses any outputs except fatal \
                    errors.\nOverrides verbosity settings."))
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .global(true)
            .value_name("FILE")
            .takes_value(true)
            .help("Sets a custom config file"))
}

// ##################################
// #       Helper functions         #
// ##################################
/// Convenience funtion to get the `scope` arguments, which are the last
/// positional arguments. Returns them as a vector of Strings.
pub fn get_scope(matches: &ArgMatches, input_name: &str) -> Vec<String> {
    let mut input = Vec::new();
    // do we get input from stdin?
    if atty::isnt(Stream::Stdin) {
        input = read_stdin();
    } else {
        // make a vector with proper strings from args.get_vec("<input>")
        if let Some(arg_input) = matches.values_of(input_name) {
            //input = arg_input.collect();
            for arg in arg_input {
                input.push(arg.to_string());
            }
        }
    }
    input
}

fn read_stdin() -> Vec<String> {
    let mut input = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        input.push(line.unwrap());
    }
    input
}

pub fn values_to_vec(values: Option<Values>) -> Vec<String> {
    let mut vector = vec![];
    
    if let Some(v) = values {
        for value in v {
            vector.push(value.to_string());
        }
    }
    vector
}
