# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Installation instructions
### Changed
### Removed
### Deprecated
### Fixed
### Security

## [0.2.0] - 2020.06.08
### Added
- Metadata for debian packages
### Changed
- examples subcommand now has flags, not sub-subcommands
- README.md - Added info about examples subcommand
- depends on libzettels 0.4.0, now
### Removed
- Some unused test code
### Fixed
- #1, Upstream bug

## [0.1.1] - 2020.06.04
Updated metadata and documentation, now that repository and crate exist.

## [0.1.0] - 2020.06.04
### Added
- Interface for libzettels API version 0.3.0
### Changed
### Deprecated
### Removed
### Fixed
### Security
